import { Entity, Input } from "../../src/decorators";
import _ from "lodash";

const countries = ["United Kingdon", "India", "Finland"];

////////// ADDRESS //////////
@Entity({ name: "Address", desc: "Your present address" })
class Address {

    @Input({ type: "VTextField", props: { label: "Street Address", hint: "This is your full street address." } })
    public streetAddress: string;

    @Input({
        type: "VTextField", filter: _.capitalize, props: {
            label: "Postcode",
            hint: "This will be auto capitalised."
        }
    })
    public postcode: string;

    constructor(obj: Address) {
        this.streetAddress = obj && obj.streetAddress ? obj.streetAddress : null;
        this.postcode = obj && obj.postcode ? obj.postcode : null;
    }
}

////////// LANGUAGE //////////
@Entity({})
class Language {
    @Input({ type: "VTextField", props: { label: "Language" } })
    public language: string | null;
    @Input({ type: "VTextField", props: { label: "Level" } })
    public level: string | null;

    constructor(obj?: Language) {
        this.language = obj && obj.language ? obj.language : null;
        this.level = obj && obj.level ? obj.level : null;
    }

    public toString() {
        return "Language: " + (this.language || "");
    }
}

@Entity({ name: "Languages", type: Language, isCollection: true, desc: "Languages you know" })
class LanguageCollection {
    [index: string]: Language;
}

////////// PERSONAL DETAILS //////////
@Entity({ name: "Personal Details", desc: "Please enter your personal details below" })
export default class PersonalDetails {

    @Input({ type: "VTextField", filter: _.startCase, props: { label: "Full Name", hint: "Auto startcase filter" } })
    public fullName: string | null;

    @Input({ type: "VSelect", props: { label: "Nationality", items: countries } })
    public nationality: string | null;

    @Input({ type: LanguageCollection, isComposite: true, props: { label: "languages" } })
    public languages: LanguageCollection;

    @Input({ type: Address, isComposite: true })
    public address: Address | null;

    @Input({ type: "VSelect", order: 1, props: { label: "Title", items: ["Mr", "Ms"] } })
    public title: string | null;

    constructor(obj?: PersonalDetails) {
        this.fullName = obj && obj.fullName ? obj.fullName : null;
        this.nationality = obj && obj.nationality ? obj.nationality : null;
        this.address = obj && obj.address ? obj.address : null;
        this.languages = obj && obj.languages ? obj.languages : null;
        this.title = obj && obj.title ? obj.title : null;
    }
}


