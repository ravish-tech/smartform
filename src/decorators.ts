import "reflect-metadata";
import log from "loglevel";

const metaDataKey = "smartForm";

export class EntityOptions {
    name?: string;
    desc?: string;
    type?: Function;
    isCollection?: boolean;
}

export function Entity(options: EntityOptions) {
    return <T extends { new(...args: any[]): {} }>(constructor: T) => {
        return class extends constructor {
            get _SmartFormEntityAttributes() { return options; }
            get _SmartFormPropertyAttributes() {
                const result: any = {};
                Object.keys(this).forEach(k => {
                    const metaData = Reflect.getMetadata(metaDataKey, this, k);
                    result[k] = new InputOptions(metaData);
                });
                return result;
            }
        }
    }
}

export class InputOptions {
    type?: string | Function;
    props?: any;
    isCollection?: boolean = false;
    isComposite?: boolean = true;
    filter?: Function;
    order?: number;
    constructor(obj?: InputOptions) {
        this.type = obj && obj.type ? obj.type : undefined;
        this.props = obj && obj.props ? obj.props : undefined;
        this.isCollection = obj && obj.isCollection ? obj.isCollection : false;
        this.isComposite = obj && obj.isComposite ? obj.isComposite : false;
        this.filter = obj && obj.filter ? obj.filter : undefined;
        this.order = obj && obj.order ? obj.order : undefined;
    }
}

export function Input(options: InputOptions) {
    return Reflect.metadata(metaDataKey, options);
}